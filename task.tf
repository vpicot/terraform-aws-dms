#-----------------------------------
# Replication Task
#-----------------------------------

resource "aws_dms_replication_task" "this" {
  for_each = { for k, v in var.replication_tasks : k => v if var.create }

  cdc_start_position        = lookup(each.value, "cdc_start_position", null)
  cdc_start_time            = lookup(each.value, "cdc_start_time", null)
  migration_type            = each.value.migration_type
  replication_instance_arn  = aws_dms_replication_instance.this[0].replication_instance_arn
  replication_task_id       = each.value.replication_task_id
  replication_task_settings = lookup(each.value, "replication_task_settings", null)
  table_mappings            = lookup(each.value, "table_mappings", null)
  source_endpoint_arn       = aws_dms_endpoint.this[each.value.source_endpoint_key].endpoint_arn
  target_endpoint_arn       = aws_dms_endpoint.this[each.value.target_endpoint_key].endpoint_arn
  start_replication_task    = lookup(each.value, "start_replication_task", null)

  tags = local.tags
}