#-----------------------------------
# Event Subscription
#-----------------------------------

resource "aws_dms_event_subscription" "this" {
  for_each = { for k, v in var.event_subscriptions : k => v if var.create }

  name             = each.value.name
  enabled          = lookup(each.value, "enabled", null)
  event_categories = lookup(each.value, "event_categories", null)
  source_type      = lookup(each.value, "source_type", null)
  source_ids = compact(concat([
    for instance in aws_dms_replication_instance.this[*] :
    instance.replication_instance_id if lookup(each.value, "instance_event_subscription_keys", null) == var.repl_instance_id
    ], [
    for task in aws_dms_replication_task.this[*] :
    task.replication_task_id if contains(lookup(each.value, "task_event_subscription_keys", []), each.key)
  ]))

  sns_topic_arn = each.value.sns_topic_arn

  tags = local.tags

  timeouts {
    create = lookup(var.event_subscription_timeouts, "create", null)
    update = lookup(var.event_subscription_timeouts, "update", null)
    delete = lookup(var.event_subscription_timeouts, "delete", null)
  }
}