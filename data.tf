# -----------------------------------
# DATA GENERAL BLOCKS
# -----------------------------------

data "aws_partition" "current" {}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Security"
    values = [upper(var.security)]
  }

  filter {
    name   = "tag:Company"
    values = [upper(var.company)]
  }
}

data "aws_subnets" "subnets_private" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc.id]
  }

  filter {
    name   = "tag:Security"
    values = [upper(var.security)]
  }

  filter {
    name   = "tag:Environment"
    values = [upper(var.environment)]
  }
}

# data "vault_generic_secret" "vault" {
#   path = var.path_vault
# }