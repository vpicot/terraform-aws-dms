locals {
  zone        = lower(var.security)
  service_id  = lower(var.service_id)
  company_    = lower(var.company)
  environment = lower(var.environment)
  freetext    = lower(var.freetext)
  name        = lower("dms-${var.company}-${var.safran_region}-${var.security}-${var.environment}-${var.service_id}-${var.freetext}")

  subnet_group_id = var.create && var.create_repl_subnet_group ? aws_dms_replication_subnet_group.this[0].id : var.repl_instance_subnet_group_id
  partition       = data.aws_partition.current.partition
  dns_suffix      = data.aws_partition.current.dns_suffix
  tags = {
    Security    = upper(var.security)
    Environment = upper(var.environment)
    Company     = upper(var.company)
    ServiceID   = upper(var.service_id)
  }
}


 