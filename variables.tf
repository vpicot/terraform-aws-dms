# -----------------------------------
# TAG MANDATORY BLOCKS
# -----------------------------------

variable "security" {
  description = "Urbasec zone"
  type        = string
  validation {
    condition     = contains(["z1", "z2", "z3", "z15"], lower(var.security))
    error_message = "Must be one of these: z1, z2, z3, z15."
  }
}
variable "environment" {
  description = "dev / npr / prd"
  type        = string
  validation {
    condition     = contains(["dev", "npd", "prd"], lower(var.environment))
    error_message = " Must be one of these: dev,npd,prd."
  }
}

variable "company" {
  description = "Tag mandatory"
  type        = string
  validation {
    condition     = length (var.company) == 3
    error_message = " Must be 3 characters long."
  }
}

variable "service_id" {
  description = "Tag mandatory eg:S105TEST"
  type        = string
}

# -----------------------------------
# NAMING BLOCKS
# -----------------------------------

variable "safran_region" {
  description = "Safran region (eu1 for Ireland or eu2 for Paris)"
  type        = string
}

variable "freetext" {
  description = "Free text for name"
  type        = string
}

# -----------------------------------
# DMS BLOCKS
# -----------------------------------

variable "region" {
  description = "AWS resource region"
  type        = string
}

variable "create" {
  description = "Determines whether resources will be created"
  type        = bool
  default     = true
}

variable "create_iam_roles" {
  description = "Determines whether the required [DMS IAM resources] will be created"
  type        = bool
  default     = true
}

variable "iam_role_permissions_boundary" {
  description = "ARN of the policy that is used to set the permissions boundary for the role"
  type        = string
  default     = null
}

variable "enable_redshift_target_permissions" {
  description = "Determines whether `redshift.amazonaws.com` is permitted access to assume the `dms-access-for-endpoint` role"
  type        = bool
  default     = false
}

variable "create_repl_subnet_group" {
  description = "Determines whether the replication subnet group will be created"
  type        = bool
  default     = true
}

variable "repl_subnet_group_description" {
  description = "The description for the subnet group"
  type        = string
  default     = null
}

variable "repl_subnet_group_subnet_ids" {
  description = "A list of the EC2 subnet IDs for the subnet group"
  type        = list(string)
  default     = []
}

variable "repl_instance_allocated_storage" {
  description = "The amount of storage (in gigabytes) to be initially allocated for the replication instance. Min: 5, Max: 6144, Default: 50"
  type        = number
  default     = null
}

variable "repl_instance_auto_minor_version_upgrade" {
  description = "Indicates that minor engine upgrades will be applied automatically to the replication instance during the maintenance window"
  type        = bool
  default     = null
}

variable "repl_instance_allow_major_version_upgrade" {
  description = "Indicates that major version upgrades are allowed"
  type        = bool
  default     = null
}

variable "repl_instance_apply_immediately" {
  description = "Indicates whether the changes should be applied immediately or during the next maintenance window"
  type        = bool
  default     = null
}

variable "repl_instance_availability_zone" {
  description = "The EC2 Availability Zone that the replication instance will be created in"
  type        = string
  default     = null
}

variable "repl_instance_engine_version" {
  description = "The [engine version] number of the replication instance"
  type        = string
  default     = null
}

variable "repl_instance_kms_key_arn" {
  description = "The Amazon Resource Name (ARN) for the KMS key that will be used to encrypt the connection parameters"
  type        = string
  default     = null
}

variable "repl_instance_multi_az" {
  description = "Specifies if the replication instance is a multi-az deployment. You cannot set the `availability_zone` parameter if the `multi_az` parameter is set to `true`"
  type        = bool
  default     = null
}

variable "repl_instance_preferred_maintenance_window" {
  description = "The weekly time range during which system maintenance can occur, in Universal Coordinated Time (UTC)"
  type        = string
  default     = null
}

variable "repl_instance_publicly_accessible" {
  description = "Specifies the accessibility options for the replication instance"
  type        = bool
  default     = null
}

variable "repl_instance_class" {
  description = "The compute and memory capacity of the replication instance as specified by the replication instance class"
  type        = string
  default     = null
}

variable "repl_instance_id" {
  description = "The replication instance identifier. This parameter is stored as a lowercase string"
  type        = string
  default     = null
}

variable "repl_instance_subnet_group_id" {
  description = "An existing subnet group to associate with the replication instance"
  type        = string
  default     = null
}

variable "repl_instance_vpc_security_group_ids" {
  description = "A list of VPC security group IDs to be used with the replication instance"
  type        = list(string)
  default     = null
}

variable "repl_instance_timeouts" {
  description = "A map of timeouts for replication instance create/update/delete operations"
  type        = map(string)
  default     = {}
}

variable "replication_tasks" {
  description = "Map of objects that define the replication tasks to be created"
  type        = any
  default     = {}
}

variable "endpoints" {
  description = "Map of objects that define the endpoints to be created"
  type        = any
  default     = {}
}

variable "event_subscriptions" {
  description = "Map of objects that define the event subscriptions to be created"
  type        = any
  default     = {}
}

variable "event_subscription_timeouts" {
  description = "A map of timeouts for event subscription create/update/delete operations"
  type        = map(string)
  default     = {}
}

variable "certificates" {
  description = "Map of objects that define the certificates to be created"
  type        = map(any)
  default     = {}
}

