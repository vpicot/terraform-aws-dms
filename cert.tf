
# -----------------------------------
# Certificate
# -----------------------------------

resource "aws_dms_certificate" "this" {
  for_each = { for k, v in var.certificates : k => v if var.create }

  certificate_id     = each.value.certificate_id
  certificate_pem    = lookup(each.value, "certificate_pem", null)
  certificate_wallet = lookup(each.value, "certificate_wallet", null)

  tags = local.tags
}