#-----------------------------------
# Subnet group
#-----------------------------------

resource "aws_dms_replication_subnet_group" "this" {
  count = var.create && var.create_repl_subnet_group ? 1 : 0

  replication_subnet_group_id          = ("repnetgp-${local.name}")
  replication_subnet_group_description = var.repl_subnet_group_description
  subnet_ids                           = data.aws_subnets.subnets_private.ids

  tags = local.tags

  depends_on = [time_sleep.wait_for_dependency_resources]
}