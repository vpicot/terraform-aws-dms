#-----------------------------------
# Endpoint
#-----------------------------------

resource "aws_dms_endpoint" "this" {
  for_each = { for k, v in var.endpoints : k => v if var.create }

  certificate_arn             = try(aws_dms_certificate.this[each.value.certificate_key].certificate_arn, null)
  database_name               = lookup(each.value, "database_name", null)
  endpoint_id                 = each.value.endpoint_id
  endpoint_type               = each.value.endpoint_type
  engine_name                 = each.value.engine_name
  extra_connection_attributes = lookup(each.value, "extra_connection_attributes", null)
  kms_key_arn                 = lookup(each.value, "kms_key_arn", null)
  password                    = lookup(each.value, "password", null)
  port                        = lookup(each.value, "port", null)
  server_name                 = lookup(each.value, "server_name", null)
  service_access_role         = lookup(each.value, "service_access_role", null)
  ssl_mode                    = lookup(each.value, "ssl_mode", null)
  username                    = lookup(each.value, "username", null)

  # https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Source.MongoDB.html
  dynamic "mongodb_settings" {
    for_each = length(lookup(each.value, "mongodb_settings", {})) == 0 ? [] : [each.value.mongodb_settings]

    content {
      auth_mechanism      = lookup(mongodb_settings.value, "auth_mechanism", null)
      auth_source         = lookup(mongodb_settings.value, "auth_source", null)
      auth_type           = lookup(mongodb_settings.value, "auth_type", null)
      docs_to_investigate = lookup(mongodb_settings.value, "docs_to_investigate", null)
      extract_doc_id      = lookup(mongodb_settings.value, "extract_doc_id", null)
      nesting_level       = lookup(mongodb_settings.value, "nesting_level", null)
    }
  }

  # https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Source.S3.html
  # https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Target.S3.html
  dynamic "s3_settings" {
    for_each = length(lookup(each.value, "s3_settings", {})) == 0 ? [] : [each.value.s3_settings]

    content {
      add_column_name                   = lookup(s3_settings.value, "add_column_name", null)
      bucket_folder                     = lookup(s3_settings.value, "bucket_folder", null)
      bucket_name                       = lookup(s3_settings.value, "bucket_name", null)
      canned_acl_for_objects            = lookup(s3_settings.value, "canned_acl_for_objects", null)
      cdc_inserts_and_updates           = lookup(s3_settings.value, "cdc_inserts_and_updates", null)
      cdc_inserts_only                  = lookup(s3_settings.value, "cdc_inserts_only", null)
      cdc_max_batch_interval            = lookup(s3_settings.value, "cdc_max_batch_interval", null)
      cdc_min_file_size                 = lookup(s3_settings.value, "cdc_min_file_size", null)
      cdc_path                          = lookup(s3_settings.value, "cdc_path", null)
      compression_type                  = lookup(s3_settings.value, "compression_type", null)
      csv_delimiter                     = lookup(s3_settings.value, "csv_delimiter", null)
      csv_no_sup_value                  = lookup(s3_settings.value, "csv_no_sup_value", null)
      csv_null_value                    = lookup(s3_settings.value, "csv_null_value", null)
      csv_row_delimiter                 = lookup(s3_settings.value, "csv_row_delimiter", null)
      data_format                       = lookup(s3_settings.value, "data_format", null)
      data_page_size                    = lookup(s3_settings.value, "data_page_size", null)
      date_partition_delimiter          = lookup(s3_settings.value, "date_partition_delimiter", null)
      date_partition_enabled            = lookup(s3_settings.value, "date_partition_enabled", null)
      date_partition_sequence           = lookup(s3_settings.value, "date_partition_sequence", null)
      dict_page_size_limit              = lookup(s3_settings.value, "dict_page_size_limit", null)
      enable_statistics                 = lookup(s3_settings.value, "enable_statistics", null)
      encoding_type                     = lookup(s3_settings.value, "encoding_type", null)
      encryption_mode                   = lookup(s3_settings.value, "encryption_mode", null)
      external_table_definition         = lookup(s3_settings.value, "external_table_definition", null)
      ignore_headers_row                = lookup(s3_settings.value, "ignore_headers_row", null)
      include_op_for_full_load          = lookup(s3_settings.value, "include_op_for_full_load", null)
      max_file_size                     = lookup(s3_settings.value, "max_file_size", null)
      parquet_timestamp_in_millisecond  = lookup(s3_settings.value, "parquet_timestamp_in_millisecond", null)
      parquet_version                   = lookup(s3_settings.value, "parquet_version", null)
      preserve_transactions             = lookup(s3_settings.value, "preserve_transactions", null)
      rfc_4180                          = lookup(s3_settings.value, "rfc_4180", null)
      row_group_length                  = lookup(s3_settings.value, "row_group_length", null)
      server_side_encryption_kms_key_id = lookup(s3_settings.value, "server_side_encryption_kms_key_id", null)
      service_access_role_arn           = lookup(s3_settings.value, "service_access_role_arn", null)
      timestamp_column_name             = lookup(s3_settings.value, "timestamp_column_name", null)
      use_csv_no_sup_value              = lookup(s3_settings.value, "use_csv_no_sup_value", null)
    }
  }

  tags = local.tags

  depends_on = [aws_dms_replication_instance.this]
}